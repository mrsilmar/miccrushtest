﻿using UnityEngine;
using System.Collections;
using System.IO;
using Fabric.Crashlytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasButtons : MonoBehaviour {

    public AudioSource aus;
    public InputField inpTime;
    public Text txtFile;
    public Button btnRec;

    public Color clrRecStart = Color.white;
    public Color clrRecIsPlaying = Color.red;
    
    public void OnBtnRecord() {
        if (btnRec.image.color == clrRecStart) {
            btnRec.image.color = clrRecIsPlaying;
            Mic.me.RecordStart(int.Parse(inpTime.text));
        } 
        
    }

    public void OnBtnStopAndSave() {
        if (btnRec.image.color == clrRecIsPlaying) {
            btnRec.image.color = clrRecStart;
            string filename = Mic.me.RecordStopAndSave();
            txtFile.text = "Record file path:" + Path.Combine(Application.persistentDataPath, filename);
        }
    }

     public void OnBtnLoadAndPlay() {
         StartCoroutine(Mic.me.Load(SceneManager.GetActiveScene().name+".wav",aus));
     }


    public void OnBtnCrash() {
        Crashlytics.Crash();
    }
    public void OnBtnThrowNonFatal() {
        Crashlytics.ThrowNonFatal();
    }

    


}
