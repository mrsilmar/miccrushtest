﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;


public class Mic : MonoBehaviour {

    public static Mic me { get; private set; }    

    private AudioClip myAudioClip;
  
    private AudioSource _auso;   

    void Awake() {
        DontDestroyOnLoad(this.gameObject);

        if (me != null && me != this) {
            Destroy(gameObject);		
        }
        me = this;
    }

    void Start() {
        _auso = GetComponent <AudioSource>();          
    }

    public void RecordStart(int duration) {
        myAudioClip = Microphone.Start( null, false, duration, 44100);
    }

    public string RecordStopAndSave() {
        string filename = string.Format( SceneManager.GetActiveScene().name);
        myAudioClip = SavWav.TrimSilence(myAudioClip, 0);
        SavWav.Save(filename,myAudioClip);
        return filename+".wav";
    }

    public IEnumerator  Load(string filename, AudioSource aus){        
        string fullFileNames="file:///" + Path.Combine(Application.persistentDataPath, filename);
        WWW www = new WWW(fullFileNames);
        yield return www;
        AudioClip tempClip = www.GetAudioClip(false);
        tempClip.name = filename;
        aus.clip = tempClip;
        aus.Play();
    }
    

}
